# mirrors-in-china
顾名思义,国内的镜像站.

这里做一个汇总,以便查找和使用.

不断更新中...

可以通过[`issue`](https://gitee.com/taadis/mirrors-in-china/issues)或[`pull request`](https://gitee.com/taadis/mirrors-in-china/pulls)来补充哦.

## 国内镜像站列表
> 排名不分先后

[网易开源镜像站 - http://mirrors.163.com/](http://mirrors.163.com/)

[阿里云开源镜像站 - http://mirrors.aliyun.com/](http://mirrors.aliyun.com/)

[腾讯云软件源 - https://mirrors.cloud.tencent.com/](https://mirrors.cloud.tencent.com/)

[Ubuntu官方中國(目前是阿里云) - http://cn.archive.ubuntu.com/ubuntu/](http://cn.archive.ubuntu.com/ubuntu/)

[搜狐开源镜像站 - http://mirrors.sohu.com/](http://mirrors.sohu.com/)

[公云开源镜像站 - http://mirrors.pubyun.com/](http://mirrors.pubyun.com/)

[开源镜像文件搜索 - http://www.mirrors.org.cn/](http://www.mirrors.org.cn/)

[首都在线开源镜像站 - http://mirrors.yun-idc.com/](http://mirrors.yun-idc.com/)

[linux运维派开源镜像站 - http://mirrors.skyshe.cn/](http://mirrors.skyshe.cn/)

[清华大学开源软件镜像站 - https://mirrors.tuna.tsinghua.edu.cn/](https://mirrors.tuna.tsinghua.edu.cn/)

[中科院开源镜像站 - http://mirrors.opencas.cn/](http://mirrors.opencas.cn/)

[中国科学技术大学开源软件镜像 - http://mirrors.ustc.edu.cn/](http://mirrors.ustc.edu.cn/)

[北京理工大学开源镜像站 - http://mirror.bit.edu.cn/web/](http://mirror.bit.edu.cn/web/)

[北京交通大学开源镜像站 - http://mirror.bjtu.edu.cn/cn/](http://mirror.bjtu.edu.cn/cn/)

[浙江大学开源镜像站 - http://mirrors.zju.edu.cn/](http://mirrors.zju.edu.cn/)

[上海交通大学镜像站 - http://ftp.sjtu.edu.cn/](http://ftp.sjtu.edu.cn/)

[南京大学开源软件镜像站 - http://mirrors.nju.edu.cn/](http://mirrors.nju.edu.cn/)

[兰州大学开源镜像站 - http://mirror.lzu.edu.cn/](http://mirror.lzu.edu.cn/)

[厦门大学开源镜像站 - http://mirrors.xmu.edu.cn/](http://mirrors.xmu.edu.cn/)

[东北大学开源镜像站 - http://mirror.neu.edu.cn/](http://mirror.neu.edu.cn/)

[华中科技大学开源镜像站 - http://mirrors.hust.edu.cn/](http://mirrors.hust.edu.cn/)

[重庆大学开源软件镜像站 - http://mirrors.cqu.edu.cn/](http://mirrors.cqu.edu.cn/)

[东软信息学院开源镜像站 - http://mirrors.neusoft.edu.cn/](http://mirrors.neusoft.edu.cn/)

[大连理工大学开源镜像站 - http://mirror.dlut.edu.cn/](http://mirror.dlut.edu.cn/)

[中山大学开源镜像站 - http://mirror.sysu.edu.cn/](http://mirror.sysu.edu.cn/)

[香港中文大学更新服务器 - http://ftp.cuhk.edu.hk/pub/Linux/](http://ftp.cuhk.edu.hk/pub/Linux/)

[平安云开源镜像站 - https://mirrors.pinganyun.com/](https://mirrors.pinganyun.com/)

[西安交通大学软件镜像站 - https://mirrors.xjtu.edu.cn/](https://mirrors.xjtu.edu.cn/)